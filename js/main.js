
/**
 * Created by luisjr on 4/13/15.
 */
//vars
var nav, navFullSize, navMin, navMax, navSize, numSections, navOffset = 240, navSelection;
var content, lastPosY, startPosY;
var overlaySection, overlayActive, disableValidation;
var idleTimeout, idleTimeoutTime = 25000, idleTimeoutDelay = 10000, disableIdleTimeout;
var pageW, pageH;
var keyboardToggle,activeTextField,shiftToggle;

//touch/mouse event normalizers
(function() {

    /* == GLOBAL DECLERATIONS == */
    TouchMouseEvent = {
        DOWN: "touchmousedown",
        UP: "touchmouseup",
        MOVE: "touchmousemove"
    }

    /* == EVENT LISTENERS == */
    var onMouseEvent = function(event) {
        var type;

        switch (event.type) {
            case "mousedown": type = TouchMouseEvent.DOWN; break;
            case "mouseup":   type = TouchMouseEvent.UP;   break;
            case "mousemove": type = TouchMouseEvent.MOVE; break;
            default:
                return;
        }

        var touchMouseEvent = normalizeEvent(type, event, event.pageX, event.pageY);
        $(event.target).trigger(touchMouseEvent);
    }

    var onTouchEvent = function(event) {
        var type;

        switch (event.type) {
            case "touchstart": type = TouchMouseEvent.DOWN; break;
            case "touchend":   type = TouchMouseEvent.UP;   break;
            case "touchmove":  type = TouchMouseEvent.MOVE; break;
            default:
                return;
        }

        var touch = event.originalEvent.touches[0];
        var touchMouseEvent;

        if (type == TouchMouseEvent.UP)
            touchMouseEvent = normalizeEvent(type, event, null, null);
        else
            touchMouseEvent = normalizeEvent(type, event, touch.pageX, touch.pageY);

        $(event.target).trigger(touchMouseEvent);
    }

    /* == NORMALIZE == */
    var normalizeEvent = function(type, original, x, y) {
        return $.Event(type, {
            pageX: x,
            pageY: y,
            originalEvent: original
        });
    }

    /* == LISTEN TO ORIGINAL EVENT == */
    var jQueryDocument = $(document);

    if ("ontouchstart" in window) {
        jQueryDocument.on("touchstart", onTouchEvent);
        jQueryDocument.on("touchmove", onTouchEvent);
        jQueryDocument.on("touchend", onTouchEvent);
    } else {
        jQueryDocument.on("mousedown", onMouseEvent);
        jQueryDocument.on("mouseup", onMouseEvent);
        jQueryDocument.on("mousemove", onMouseEvent);
    }

})();

//constants
var KEYBOARD = {
    "q": "1",
    "w": "2",
    "e": "3",
    "r": "4",
    "t": "5",
    "y": "6",
    "u": "7",
    "i": "8",
    "o": "9",
    "p": "0",
    "a": "/",
    "s": ":",
    "d": ";",
    "f": "(",
    "g": ")",
    "h": "$",
    "j": "&",
    "k": "%",
    "l": "#",
    "c": ",",
    "v": "?",
    "b": "!",
    "n": "'",
    "m": "\"",
    "z": "undo"
}

function init(){

    /* DEBUG */
    //disableIdleTimeout = disableValidation = true;

    nav = document.getElementById("nav");
    content = document.getElementById("content");
    var data = DATA;
    numSections = data.length;

    //init the nav + sections
    for(var i = 0; i < numSections; i++) {


        var item = data[i];
        /*JSON object
         item.title
         item.fgcolor
         item.bgcolor
         item.subtitle
         item.copy
         item.icon*/

        //create the content section
        var li = $("<li />");

        //change font color based on background
        if(item.bgcolor==="FFFFFF" || item.bgcolor==="FFE000" || item.bgcolor==="BAD636")
            $(li).addClass("black");

        //add div content
        li
            .css("background-color", "#" + item.bgcolor)
            .append("<h2>" + parseLinebreaks(item.subtitle) + "</h2>")
            .append("<h1>" + parseLinebreaks(item.title) + "</h1>")
            .appendTo(content);

        //add description content
        $("<div class='description first'></div>")
            .append("<div class='box'><div class='paragraphs'>" + addLegal(parseParagraphs(item.copy)) + "</div></div>")
            .append("<button class='arrow left' ></button>")
            .append("<button class='arrow right'></button>")
            .appendTo(li);

        //add learn more button
        $("<button class='learn'>Learn More</button>")
            .css("background-color", item.bgcolor==="0076BF"? "#ffffff": "#" + item.fgcolor)
            .css("color", item.bgcolor==="0076BF"? "#0076BF": (item.bgcolor==="#000000"?"000000":"#ffffff"))
            .appendTo(li);

        //create the nav button
        li = $("<li><div class='spacer'></div><div class='block'></div></li>")
            .css("background-color", "#" + item.bgcolor)
            .css("background-image", "url(img/c_icon00" + item.icon +".png)")
            .appendTo(nav);

        //create category for interest list
        li = $("<li><div class='check'></div><div class='title'>"+item.title+"</div></li>")
            .appendTo("#categories")
            .on(TouchMouseEvent.UP, function(event){

                if($(this).hasClass("autoselected")) return;
                $(this).toggleClass("selected");
            });

    }

    //duplicate nav/sections for endless scrolling
    $("#nav li")
        .clone()
        .appendTo(nav);

    $("#content li")
        .clone()
        .appendTo(content);

    //reposition titles based on lines
    $("h1").each(function(index, element){
        var h = $(element).outerHeight();
        if(h > 300) $(element).addClass("l3");
    });

    //reposition subtitles based on lines
    $("h2").each(function(index, element){
        var h = $(element).outerHeight();
        if(h > 30) $(element).addClass("l2")
    });

    //add arrow functionality
    $("button.arrow").on( TouchMouseEvent.DOWN, function(event){

        //console.log("butt", event.target, event.currentTarget);

        resetIdle();

        event.preventDefault();
        event.stopImmediatePropagation();

        var bu = event.target;
        showNextDescriptor(bu.parentNode, $(bu).hasClass("left")? -1: 1);

    });

    //learn button actions
    $("button.learn").on( TouchMouseEvent.UP, function(event){

        showOverlay();

    })

    //submit button handler
    $("#overlay button.submit").on(TouchMouseEvent.UP, submitHandler);

    //close button handler
    $("#overlay button.close").on(TouchMouseEvent.UP, function(event){

        hideOverlay();
    });

    //description swipe
    $("div.description").on(TouchMouseEvent.DOWN, function(event){

        resetIdle();

        var co = $(this).find("div.paragraphs");
        var el = co.get(0);

        var lastPosX = event.pageX;
        var initPosX = el._gsTransform?el._gsTransform.x:0;

        //add move/up handlers after the down handler
        $(document)
            .on(TouchMouseEvent.MOVE, function(event){

                var posX = event.pageX-lastPosX+initPosX;
                TweenMax.set(co, {x: posX });

            })
            .on(TouchMouseEvent.UP, function(event){

                $(document)
                    .off(TouchMouseEvent.MOVE)
                    .off(TouchMouseEvent.UP);

                var delta = event.pageX - lastPosX;
                if(Math.abs(delta) < 10) delta = 0;
                else delta = delta > 0? -1: 1;

                showNextDescriptor(el.parentNode.parentNode, delta);

            });
    })

    //input fields
    $("form div").each(function(index, element){

        var placeholder = $(element).data("placeholder");

        $(element).on(TouchMouseEvent.UP, function(event){

            $("form div").removeClass("active");

            activeTextField = element.getAttribute("name");
            $(this).addClass("active");

        })
            .append("<span class='copy'></span><span class='placeholder'>"+ placeholder +"</span>");

    });

    //mousedown on page
    $(document).on(TouchMouseEvent.DOWN, function(event){

        //reset idle on every touch
        resetIdle();

        if(overlayActive) return;

        //if user hasn't touched nav to scroll, move along
        var element = event.target;
        var parent = element.parentNode;
        if(parent != nav && parent != content) return;

        //init our touch stuff
        var initPosY = event.pageY, lastPosY = initPosY, startPosY = nav._gsTransform.y;
        var deltaY = 0, offsetY = 0, startContPosY = content._gsTransform.y;

        //add mousemove handler
        $(document).on(TouchMouseEvent.MOVE, function(event){

            var pageY = event.pageY;
            deltaY = lastPosY - pageY;
            offsetY = pageY - initPosY;

            if(parent == nav) {
                TweenMax.set(nav, {y: (offsetY + startPosY)});
                TweenMax.set(content, {y: (offsetY + startPosY - navOffset) * pageH / navSize});
            }else{
                TweenMax.set(content, {y: (offsetY + startContPosY)});
                TweenMax.set(nav, {y: (offsetY + startContPosY) * navSize / pageH + navOffset});

            }
            lastPosY = pageY;

        });

        //add mouseup handler
        $(document).on(TouchMouseEvent.UP, function(event){

            $(document).off(TouchMouseEvent.MOVE);
            $(document).off(TouchMouseEvent.UP);

            //find out if we have enough to throw
            var endPosY;

            //check if its moved
            if(Math.abs(offsetY) < 2){

                //its a click
                if(event.target != element) return;

                //get index
                var index = Array.prototype.indexOf.call(element.parentNode.children, element);
                var selected = nav.childNodes[index];

                endPosY = -$(selected).position().top;

            }
            else
            {

                //clamp that delta
                var delta = -offsetY + deltaY * 20;

                if(delta > navFullSize) delta = navFullSize
                else if(delta < -navFullSize) delta = -navFullSize;

                var val = (startPosY - delta)/ navSize;
                endPosY =  Math.floor(val) * navSize;

            }

            slideTo(endPosY+navOffset);

        });

    });

    //default motion items
    TweenMax.set("#nav,#content,#content li div.paragraphs", {x: 0});

    //init the keyboard
    updateKeyboard(true);

    $(document).bind('mousewheel', function(e){

        if(overlayActive) return;

        resetIdle();

        var startPosY = nav._gsTransform.y;
        var delta = -e.originalEvent.wheelDelta*2;

        if(delta > navFullSize) delta = navFullSize
        else if(delta < -navFullSize) delta = -navFullSize;

        var val = (startPosY - delta)/ navSize;
        var endPosY =  Math.floor(val) * navSize;

        slideTo(endPosY+navOffset);

    });

    //size it up
    $(window).trigger("resize");

}

$(document).ready(init);
$(window).load(intro);
$(window).resize(function(event){

    navSize = $(nav).innerWidth();
    navFullSize = numSections * navSize;

    pageW = 1920;
    pageH = 1080;

    navMin = -navFullSize / 4;
    navMax = navMin - navFullSize;


});

//intro animation
function intro(){

    //show the list
    $("body").css("visibility", "visible");
    TweenMax.from("body", 1.0, {opacity: 0});

    var prev = nav.childNodes[Math.round(Math.random()*6)];
    var selected = nav.childNodes[Math.round(Math.random()*6)+6];

    TweenMax.set(nav, {y: -$(prev).position().top});
    slideTo(-$(selected).position().top+navOffset, 1.5, Back.easeOut);

    resetIdle();

}

//slide function
function slideTo(endY, time, ease){

    var startY = nav._gsTransform.y;

    while(endY > navMin){
        startY -= navFullSize;
        endY -= navFullSize;
    }
    while(endY < navMax){
        startY += navFullSize;
        endY += navFullSize;
    }

    time = time || Math.max(0.7, Math.abs(endY - startY) / 1200);

    if(!ease) ease = Back.easeOut;

    TweenMax.fromTo(nav, time, {y: startY}, {y: endY, ease: ease, easeParams:[0.5]});
    TweenMax.fromTo(content, time, {y: (startY-navOffset) * pageH/navSize}, {y: ((endY-navOffset) * pageH/navSize), ease: ease, easeParams:[0.5]})

    var selection = Math.floor((endY-navOffset) / -navSize);

    TweenMax.to("#logo img.white", time*0.5, {delay: time*0.25, opacity: $(content.childNodes[selection]).hasClass("black")?0:1});

    if(selection !=navSelection) {
        navSelection = selection;
        resetContent(true, time);
    }

}

function resetContent(animate, time){

    var el;
    el = $(content.childNodes[navSelection])
        .find("div.description")
        .removeClass("last first")
        .addClass("first")
        .find("div.paragraphs")


    TweenMax.set(el, {x:0});
    el.data("p", 0);

}

//parses the description copy into byte-size paragraphs
function parseParagraphs(copy){
    return "<p>"+ parseLinebreaks(copy.split("\n\n").join("</p><p>")) + "</p>";
}

//parses line
function parseLinebreaks(copy){
    return copy.split("\n").join("<br/>");
}

function showOverlay(){

    overlaySection = 0;
    activeTextField = "name";
    overlayActive = true;

    $("#categories li").removeClass("autoselected selected");
    $("#overlay h3.header").css("visibility", "visible");

    $("#overlay section").css("display", "none");
    $("#overlay,#overlay section.userinfo").css("display", "block");

    TweenMax.set("#overlay section.userinfo", {x: "0%"});

    $("form div").removeClass("active").removeClass("populated").eq(0).addClass("active");
    $("form div span.copy").text("");
    $("form div span.error").remove();

    TweenMax.to("#content li *", 0.5, {opacity: 0});
    TweenMax.to("#overlay", 0.4, {width: "100%", delay:0.25, ease: Quad.easeIn});

}

function hideOverlay(){
    TweenMax.to("#overlay", 0.3, {width: 0, ease: Quad.easeIn, onComplete: function(){
        $("#overlay").css("display", "none");
        overlayActive = false;
    }});
    TweenMax.to("#content li *", 0.5, {opacity: 1, delay:0.25});

    resetContent();

}

function resetIdle(){

    clearTimeout(idleTimeout);
    if(disableIdleTimeout) return;

    idleTimeout = setTimeout(idle, idleTimeoutTime);
}

function idle() {

    if (overlayActive)
        hideOverlay();

    clearTimeout(idleTimeout);
    idleTimeout = setTimeout(idle, idleTimeoutDelay);

    var pos = navSelection + (Math.round(Math.random() * 4) + 2) * (Math.random() < 0.5 ? -1 : 1);
    slideTo(-pos * navSize + navOffset, 2, Back.easeInOut);

}

function keyboardPress(event){

    $("form span.error").remove();

    var t = event.target;
    var c = t.className;
    var f = $("form div[name="+ activeTextField + "]");
    var e = $("form div[name="+ activeTextField + "] span.copy");
    var v = e.text();

    $(t).css("background-color", "#ffffff");//"opacity", 1);


    var output;

    switch(c){
        case "bs": //backspace

            output = v.substr(0, v.length-1);

            break;

        case "sh": //shift

            shiftToggle = !shiftToggle;
            updateKeyboard();

            break;

        case "sy": //symbol

            keyboardToggle = !keyboardToggle;
            $("#keyboard").toggleClass("symbols");
            updateKeyboard();

            break;

        case "sb": //spacebar

            output = v+" ";

            break;

        case "dc":

            output = v+".com";

            break;

        default:

            output = (keyboardToggle && KEYBOARD[c]? KEYBOARD[c]: c);

            if(c == "z" && keyboardToggle){
                output = v = output = "";
            }

            if(shiftToggle) output = output.toUpperCase();
            shiftToggle = false;
            updateKeyboard();

            output = v + output;

    }

    if(output == null) return;
    e.text(output);

    if(output.length == 0){
        f.removeClass("populated")
    }else{
        f.addClass("populated")
    }

}

function updateKeyboard(initial){

    $("#keyboard button").each(function(index, element) {

        var c = element.className;

        if (c.length === 1) {

            $(element).text((keyboardToggle && KEYBOARD[c]? KEYBOARD[c]: c).toUpperCase());
        }

        if(initial){
            $(element)
                .on(TouchMouseEvent.UP, keyboardPress)
                .on(TouchMouseEvent.DOWN, function(event){
                    $(event.target).css("background-color", "#ddddff");
                });
        }
    });

    $("#keyboard button.sh").css("background-color",shiftToggle? "#ddddff":"#ffffff");
    $("#keyboard button.sy").text(keyboardToggle?"ABC":".?123");

}

function showNextDescriptor(li, dir){

    var co = $(li).find("div.paragraphs");
    var pa = $(co).find("p");
    var pi = co.data("p") || 0;

    pi+= dir;
    if(pi <= 0) {

        pi = 0;
        $(li).addClass("first");


    } else if(pi >= pa.length-1) {

        pi = pa.length - 1;
        $(li).addClass("last");


    } else {
        $(li).removeClass("first last");
    }

    co.data("p", pi);
    TweenMax.to(co, 0.5, {x: -pa.get(pi).offsetLeft});

}

function getFormValue(name){

    return $("form div[name='"+name+"'] span.copy").text();

}

function submitHandler(event){

    resetIdle();

    event.preventDefault();
    event.stopImmediatePropagation();

    if(overlaySection == 0 && !disableValidation){

        //validate
        var name = getFormValue("name");
        var email = getFormValue("email");
        var org = getFormValue("org");

        var errors = true;

        if(name.length == 0){
            $("form div[name='name']").append("<span class='error'>Please enter your name</span>");

        }else if(email.length == 0){
            $("form div[name='email']").append("<span class='error'>Please enter your email address</span>");

        }else if(!validateEmail(email)){
            $("form div[name='email']").append("<span class='error'>Please enter a valid email address</span>");

        }else {
           errors = false;
        }

        if(errors) return;

    }

    var currentSection = $("#overlay section").eq(overlaySection++);
    var newSection = $("#overlay section").eq(overlaySection);

    TweenMax.fromTo(currentSection, 0.6, {x:"0%"}, {ease: Cubic.easeInOut, x: "-100%", onComplete:function(){

        $(currentSection).css("display", "none");

    }, clearProps:"transform"});
    TweenMax.fromTo(newSection, 0.6, {x:"100%"}, {ease: Cubic.easeInOut, x: "0%", onStart:function(){

        $(newSection).css("display", "block");
        $(newSection).find("h3.header").css("visibility", "hidden");
        $(currentSection).find("h3.header").css("visibility", "visible");


    }, clearProps:"transform"});

    //hide title in last section
    $("#overlay h3.header")
        .css("visibility", overlaySection!==2?"visible":"hidden");


    if(overlaySection==1){

        //set active, unselectable thing
        $("#categories li").eq(navSelection%numSections).addClass("autoselected");


    }

    //this is for the last section
    if(overlaySection==2){

        //create interest array
        var selection = [];
        var initSelection = $("#categories li.autoselected").text();
        $("#categories li.selected").each(function(index, element){
            selection.push($(element).find("div.title").text())
        });

        //get saved submissions
        var submissions = localStorage["submissions"];
        var storedSubmissions = submissions? JSON.parse(submissions) : [];
        var date = new Date();

        //create our submission
        var submission = {
            name: getFormValue("name"),
            email: getFormValue("email"),
            organization: getFormValue("org"),
            entry_solution: initSelection,
            solutions_requested: selection,
            date: (date.getMonth()+1)+"/"+date.getDay()+"/"+date.getYear(),
            time: date.getHours()+":"+date.getMinutes()+":"+date.getSeconds(),
            timestamp: date.valueOf()
        }

        //save our submissions
        storedSubmissions.push(submission);
        localStorage["submissions"] = JSON.stringify(storedSubmissions);

        //append to the selection copy
        $("#interest_info").html("<li>"+initSelection+"</li>" + (selection.length?"<li>"+selection.join("</li><li>")+"</li>":""));

        setTimeout(hideOverlay, 5000);
    }
}

function shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex ;

    while (0 !== currentIndex) {

        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function addLegal(text){
    return text.replace(/Together we can make it happen./g, 'Together we can make it happen.<sup>™</sup>');
}