DATA =
[{
  "title": "Automated Systems Optimization ",
  "fgcolor": "FD4F57",
  "bgcolor": "454794",
  "icon": "01",
  "subtitle": "Increasing capacity while decreasing downtime.",
  "copy": "Your huge investment in automated equipment can be brought to its knees by pallets that don’t work in sync with your system.\n\nThe Automated Systems Optimization solution from CHEP ensures that you get the most out of your equipment and your entire supply chain.\n\nIt’s a simple, actionable way to gain capacity from your network without adding actual capacity.\n\nIt’s how you ensure that all your robotics and automated equipment are running in harmony with your shipping platforms, no matter what they are.\n\nIt means more uptime and less downtime. It means more capacity and fewer risks. Along with reduced product damage and decreased downtime.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
  {
    "title": "Unit Load Optimization",
    "fgcolor": "00BCDD",
    "bgcolor": "FFE000",
    "icon": "02",
    "subtitle": "Shipping more for less.",
    "copy": "A small improvement in how much product you can fit on a pallet can lead to significant cost reductions.\n\nYou can get these savings — savings that lower labor costs, handling costs, pallet costs and transport costs — often with no operational change.\n\nWe make Unit Load Optimization simple. Our process is customized, efficient and cost -effective. Many companies see an ROI of up to 400%.\n\nOnly CHEP combines the expertise of our experienced engineers, state of the art software and access to testing at CHEP’s ISTA -certified Innovation Center.\n\n CHEP brings together two unique capabilities — unit load configuration and packaging testing – in one solution.\n\nTogether they can help you configure maximum load efficiency for every load and test configurations to ensure real world success.\n\nThis is the Supply Change.\nTogether we can make it happen."
  },
{
    "title": "Platform Management",
    "fgcolor": "BAD636",
    "bgcolor": "000000",
  "icon": "06",
    "subtitle": "Managing your core business, not your shipping platforms.",
    "copy":"Whether you want to better re use pallets, increase their value or reduce the their procurement cost, your operation will run better with CHEP people inside.\n\nImagine how much simpler your business will be when your people no longer have to sort pallets, repair pallets, track, manage and inventory.\n\nWith our people on site, our customers experience average procurement savings of 7.5% on pooled pallets and 20% savings on recycled pallets.\n\nOur onsite pallet -experts can take on every\naspect of platform management, including\nsortation, repairing, and returning of platforms\nto inventory for usage.\n\nWe work with every type of platform: pooled pallets, recycled pallets, containers, and proprietary platforms.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "Store\nFulfillment",
    "fgcolor": "A35398",
    "bgcolor": "FF893D",
  "icon": "07",
    "subtitle": "Inventing. Innovating. Implementing.",
    "copy": "We’ll help you get products onto shelves quicker, more efficiently and more in front of your customers. We’ll help you handle multiple store formats.\n\nAll your stores can be on the cutting edge of innovation. Helping you drive sales, efficiency, customer retention.\n\nWe’ll collaborate with you and take the best of what we’ve learned from around the world and help adapt it to your needs.\n\nWe can pilot breakthroughs in one store and\nthen apply our know how and vast resources to\nincorporate those innovations throughout your\nentire ecosystem.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "Platform Mix Optimization",
    "fgcolor": "00BCDD",
    "bgcolor": "FFE000",
  "icon": "08",
    "subtitle": "The right platform for the job.\nNo matter what the job." ,
    "copy" : "Platform Mix Optimization delivers efficiency, cost savings and less product damage. It means your entire supply chain functions better.\n\nFinding the right mix of platforms is one of the greatest, most immediate ways to save costs within your supply chain.\n\nCHEP offers pooled pallets, recycled pallets,\nfractional pallets, custom pallets, containers\nand RPCs.\n\nWe can zero -in on finding the right platforms for your business at the greatest possible savings.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "Supplier Diversity",
    "fgcolor": "FF893D",
    "bgcolor": "BAD636",
  "icon": "09",
    "subtitle": "Making a difference for diversity and your business.",
    "copy": "For most companies today, having a Supplier Diversity program is not just “the right thing to do.” It’s virtually an imperative.\n\nChanging demographics and customer mandates have led to the realization that diversity is more than a social responsibility – it drives business value.\n\nSupplier Diversity is central to our core strategy – to be our customers’ supplier of choice.\n\nWe utilize diverse suppliers within our supply chain and to bring value to our customers by helping them create or enhance their own programs.\n\nOver the past four years, we’ve increased our spending by over $58 million. By 2017, it’s projected we’ll spend $100 million with diverse suppliers.\n\nWe’ll work with you to make your Supplier Diversity program work for your company and for the community.\n\nWe can help you optimize the program you already have in place. Or we can help you by creating a new Supplier Diversity initiative with you and for you.\n\nThis is the Supply Change.\nTogether we can make it happen"
},
  {
    "title": "Packaging Performance Testing",
    "fgcolor": "FD4F57",
    "bgcolor": "454794",
    "icon": "05",
    "subtitle": "Increasing efficiency. Decreasing risk.",
    "copy": "You have an idea on how to change your packaging. Now you can test that idea and make sure things won’t go wrong in market.\n\nWe can help reduce costs in multiple ways – in your primary packaging, secondary packaging, transit packaging and in shipping platforms.\n\nPlus, testing allows you to inexpensively evaluate new packaging before you incur costs implementing it. It removes risk. Prevents mistakes.\n\nWe’ll replicate and simulate real -world material handling, shipping and environmental conditions to determine the supply chain’s impact on a unit load.\n\nThis is the Supply Change.\nTogether we can make it happen."
  }, {
  "title": "Reverse Logistics",
  "fgcolor": "00BCDD",
  "bgcolor": "FFE000",
  "icon": "03",
  "subtitle": "Freeing your resources to optimize their value.",
  "copy": "You don’t want to spend your time, money and energy in places where they don’t return maximum value. You want your people, facilities and transportation assets freed to focus on where they can do the most good.\n\nCHEP’s Reverse Logistics solution can help you with a unified, nationwide infrastructure that can better monetize the back end of your supply chain.\n\nOur people, transportation and facilities can help you remove, collect and redistribute platforms and recyclables from your stores and distribution centers.\n\nOur nationwide network helps you manage salvage easier than ever. We can do all the work for you, or share the work with you.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "International Solutions",
    "fgcolor": "00CBC4",
    "bgcolor": "FFFFFF",
  "icon": "11",
    "subtitle": "Navigating better, faster, and smarter all around the world.",
    "copy": "Exporting goods to international markets can be complicated. There are rules and regulations, and the complexity of supply chain logistics.\n\nCHEP can help you navigate through the supply chain ecosystem – within the world of platforms and the logistics of international shipping.\n\nWe have the platform types, history, local market knowledge, and international infrastructure to help you implement the right overseas platform.\n\nAnd we want to help you do it in ways that are faster, better, smarter – and with less cost.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "Environmental Sustainability",
    "fgcolor": "FF893D",
    "bgcolor": "BAD636",
  "icon": "12",
    "subtitle": "Getting more from every unit load.",
    "copy": "Sustainability and cost savings are no longer mutually exclusive. We’ll find ways to reduce your supply chain’s impact on the environment.\n\nWe can show you how sustainability — doing what’s right for the environment — doesn’t get in the way of doing what’s right for your business.\n\nWe’ll find ways to help eliminate waste, increase your efficiencies, lower costs and use more sustainable materials.\n\nWe’ll help you figure out how to eliminate the cost of empty miles  and the fuel and CO2 that goes with them. We can reduce the amount of damaged product ends up in landfills.\n\nWe can help you reduce the trips needed to transport goods to and from your facilities. And implement more environmentally friendly, lower cost packaging.\n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
  "title": "Value Stream Mapping",
  "fgcolor": "00BCDD",
  "bgcolor": "FFE000",
  "icon": "04",
  "subtitle": "Recover money you didn’t know was lost.",
  "copy": "Value Stream Mapping gives you a complete, easy to\nunderstand graphical depiction of your entire supply chain.\n\nThis lets you find new ways to improve efficiency, product quality and overall operating margins.\n\n With your map in hand, we can help you benchmark your various facilities’ performance against one- another.\n\nWe can help you identify best practices and the root causes behind network inefficiencies. And uncover new ways to improve efficiencies and margins. \n\nThis is the Supply Change.\nTogether we can make it happen."
},
{
    "title": "Transportation Solutions",
    "fgcolor": "FF893D",
    "bgcolor": "00CBC4",
  "icon": "13",
    "subtitle": "Leveraging the power of North America’s\nlargest backhaul shipper.",
    "copy": "Optimizing your transportation network can have a\nhuge impact on your supply chain — and your\nbottom line.\n\nNow you can share assets with CHEP, North America’s largest backhaul shipper with 45,000+ unique lanes and 20,000 locations.\n\nWe can work with you to lower your transportations costs, add revenue, improve network efficiencies and even reduce the cost of your pallets.\n\nWe can help you via Fleet Optimization, Carrier Synergies, and Customer Pick up.\n\nThis is the Supply Change.\nTogether we can make it happen."},
{
    "title": "Platform Solutions",
    "fgcolor": "FFE000",
    "bgcolor": "0076BF",
  "icon": "14",
    "subtitle": "Getting more from every unit load.",
    "copy": "Platform Solutions give you everything it takes to transport your unit loads more efficiently.  Receive and design them more efficiently.\n\nWe’ve helped more companies move more goods, to more places, more efficiently and more sustainably than anyone else in the world.\n\nIn over 60 countries, we can help you do the same. Help you move more with less. Help you decrease your costs and your carbon-footprint.\n\nWith the greatest range and supply of conveyance platforms, we can provide you with the right platform at the right time at the right price.\n\nSo you can always move what you need to move when, where and how you need to, no matter what.\n\nThis is the Supply Change. \nTogether we can make it happen."
},
{
"title": "Product Damage Reduction",
"fgcolor": "00BCDD",
"bgcolor": "FFE000",
"icon": "10",
"subtitle": "Reduce damage. Reduce waste. Reduce costs.",
"copy": "The key to handling product damage is finding its root causes — zeroing in on the when, where, and why behind issues.\n\nDoes it happen due to routine movements through your network and supply chain? Environmental factors? Or automated systems and equipment?\n\nProduct damage costs manufacturers more than $5 million a day. More than that, product damage can hurt your reputation.\n\nReducing product damage can be done efficiently and cost effectively. CHEP has the knowledge and tools to help you complete the job.\n\nWe have visibility into all segments of the supply chain, an ISTA certified Innovation Center for testing, value stream mapping tools and more.\n\nThis is the Supply Change.\nTogether we can make it happen."
}
];